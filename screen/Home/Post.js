import React, { Component } from 'react';
import { Text, TextInput, View ,Button,StyleSheet,Image,ScrollView,StatusBar,TouchableOpacity,ImageBackground} from 'react-native';
import { Divider,ListItem,Icon } from 'react-native-elements';

import styles from './hstyle';

export default class Post extends Component {
  render() {
      const {text,image} = this.props;
   
    return (            
        <View>

            <TouchableOpacity>
            <View style={{flex:1,flexDirection:'row',paddingHorizontal:6 , backgroundColor:'#ffffff'}}  >
              <View style={{flex:1,marginTop:10}} >
                  <Image style={styles.imageProfile}
                  source={require('../img/prof.jpg')}
                  />
              </View>
              <View style={{flex:6,margin:10}} >
              <Text style={{fontWeight:'bold',color:'black'}} >{this.props.username}</Text>
              <Text style={{fontWeight:'100',color:'black'}} >{this.props.date}</Text>
                  </View>
              <View style={{flex:1,marginTop:15}} >
              <Image source={require('../img/dhmenu.png')} style={{width:20,height:20}} />
              </View>
              </View>
              </TouchableOpacity>

              <View style={{backgroundColor:'#ffffff'}}  >
              <TouchableOpacity>
                <View style={{marginHorizontal:9}} >
                  <Text style={{padding:5,color:'black'}} >{this.props.caption}</Text>
                </View>
                </TouchableOpacity>
                  <TouchableOpacity>                  
                   <View style={{width:"100%",width:"100%",  flexDirection:'row',flexWrap:"wrap"}} >
                    <Image source={require('../img/story.png')} style={{height:300,width:'100%',resizeMode:"cover"}} />
                </View>
        
                </TouchableOpacity>
                  
              </View>

            
                 <TouchableOpacity>
                   <View style={{flexDirection:'row',backgroundColor:'white',height:40}} >
                    <Image source={require('../img/like-react.png')} style={{width: 20,height:20}}/>
                    <View style={{justifyContent:"center"}} >
                    <Text>100</Text>
                    </View>
                </View>
                    </TouchableOpacity>

              <Divider style={{backgroundColor:'grey'}} />
              <View style={{width:'100%', height:40,backgroundColor:'#ffffff'}} >
                <View style={{flex:1,flexDirection:'row',paddingHorizontal:6,backgroundColor:'#ffffff'}} >
                
                  <View style={styles.wrapperLikeComment} >
                  <TouchableOpacity >
                  <Image source={require('../img/like-inactive.png')} style={{width: 20,height:20}}/>
                    </TouchableOpacity>
                  </View>
                
                <View style={styles.wrapperLikeComment} >
                <TouchableOpacity>
                    <Image source={require('../img/comment.png')} style={{width: 20,height:20}}/>   
                  </TouchableOpacity>
                </View>
                <View style={styles.wrapperLikeComment} >
                <TouchableOpacity>
                    <Image source={require('../img/share.png')} style={{width: 20,height:20}} />
                  </TouchableOpacity>
                
                </View>
                </View>

              
        <Divider style={{backgroundColor:'#9ea2a8',height:10}} />
            </View>
        
        
        
        
        
        </View>
    )
  }
}
