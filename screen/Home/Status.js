import React,{ Component } from 'react';
import {
    Text,
    TextInput,
    View ,
    Button,
    StyleSheet,
    Image,
    ScrollView,
    StatusBar,
    TouchableOpacity
} from 'react-native';

import styles from './hstyle';

export default class Ustatus extends Component{
    render(){
        return(
            <View style={{flexDirection:'row',paddingHorizontal:6 , backgroundColor:'#ffffff',marginTop:1,marginBottom:5}} >
            <View style={{flex:1,marginVertical:5,marginTop:8}} >
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Profil')}>
                <Image style={styles.iProfile}
                source={require('../img/prof.jpg')}
                />
                </TouchableOpacity>
            </View>
            <View style={{flex:6,margin:10}} >
                <TextInput onFocus={()=>this.props.navigation.navigate('Pstatus')} style={{borderWidth:0.5,borderRadius:50,paddingLeft:20,height:35}}  placeholder='Whats on your mind??' placeholderTextColor='black' ></TextInput>
                </View>
            <View style={{flex:1}} >
                <Image style={{width: 30, height: 30, marginBottom: 8}} 
                source={require('../img/lpost.png')}/>
                <Text style={{color:'grey',fontWeight:"100",fontSize:11.5,top:-6,left:5}} >Foto</Text>
            </View>
        </View>
        )
    }
}