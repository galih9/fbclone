import React, { Component } from 'react';
import { Text, TextInput, View ,Button,StyleSheet,Image,ScrollView,StatusBar,TouchableOpacity} from 'react-native';

import styles from './hstyle'

export default class Story extends Component {
    

  render() {
    return (
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}  >

        <View style={{backgroundColor:'#ffffff',width:'100%', height:200}}  >
       <View style={{flex:1,flexDirection:'row',paddingHorizontal:5,marginVertical:10}} >
           {/* add own story */}
         <View style={styles.wrapperImageStory} >
           <Image style={styles.imageStory} 
                 source={require('../img/prof.jpg')} />
           <Image style={{resizeMode:'cover',position:'absolute',margin:3,width:50,height:50,borderRadius:100}}
                 source={require('../img/plus.png')} />
                 <Text style={styles.namePeopleStory} >Tambahkan Ke Cerita</Text>
         </View>

                   <View style={styles.wrapperImageStory} >
                   <Image style={styles.imageStory} 
                         source={require('../img/story.png')}/>
                         <View style={styles.wrapperPublicStory} >
                       <Image style={{resizeMode:'cover',position:'absolute',width:34,height:34,borderRadius:100}}
                         source={require('../img/story.png')} />
                         </View>
                         <Text style={styles.namePeopleStory} >Name goes here</Text>
                 </View>
                 <View style={styles.wrapperImageStory} >
                   <Image style={styles.imageStory} 
                         source={require('../img/story.png')}/>
                         <View style={styles.wrapperPublicStory} >
                       <Image style={{resizeMode:'cover',position:'absolute',width:34,height:34,borderRadius:100}}
                         source={require('../img/story.png')} />
                         </View>
                         <Text style={styles.namePeopleStory} >Name goes here</Text>
                 </View><View style={styles.wrapperImageStory} >
                   <Image style={styles.imageStory} 
                         source={require('../img/story.png')}/>
                         <View style={styles.wrapperPublicStory} >
                       <Image style={{resizeMode:'cover',position:'absolute',width:34,height:34,borderRadius:100}}
                         source={require('../img/story.png')} />
                         </View>
                         <Text style={styles.namePeopleStory} >Name goes here</Text>
                 </View><View style={styles.wrapperImageStory} >
                   <Image style={styles.imageStory} 
                         source={require('../img/story.png')}/>
                         <View style={styles.wrapperPublicStory} >
                       <Image style={{resizeMode:'cover',position:'absolute',width:34,height:34,borderRadius:100}}
                         source={require('../img/story.png')} />
                         </View>
                         <Text style={styles.namePeopleStory} >Name goes here</Text>
                 </View>
         
       </View>
       </View>
        </ScrollView>
    )
  }
}