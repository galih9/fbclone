import React,{ Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    Image
} from 'react-native';

export default class WallProfile extends Component{
    render(){
        return(
            <View>
                <Image
                source={require('../img/wp.jpg')} 
                style={styles.wall}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    wall :{
        width: 320,
        height: 50,
    }
})