import {StyleSheet} from 'react-native';

export default StyleSheet.create({

    iProfile: {
        width:30,
        height:30,
        margin:5,
        borderRadius:100
    },
    wrapperLikeComment:{
      flex:1,
      alignItems:'center',
      flexDirection:'column',
      marginVertical:7
      
    },
    imageProfile:{
      width:40,
      height:40,
      margin: 5,
      borderRadius:100
    },
    likeComment:{
      width:30,
      height:24,
      marginHorizontal:5,
    },
    wrapperPublicStory:{
      margin:7,
      resizeMode:'cover',
      position:'absolute',
      borderWidth:1,
      width:40,
      height:40,
      borderRadius:100,
      borderColor:'blue',
      justifyContent:'center',
      alignContent:'center',
      alignItems:"center"
    },
    namePeopleStory:{
      color:'black',
      position:'absolute',
      fontWeight:'200',
      bottom:5,
      textAlign:'center',
      alignItems:'center',
      alignItems:"flex-end"
    },
    imageStory:{
      width:"100%",
      height:"100%",
      borderRadius:10,
      resizeMode:'cover',
      position:'relative'
    },
    wrapperImageStory:{
      backgroundColor:'blue',
      width:80,
      height:80,
      borderRadius:10,
      margin:5
    },
    container: {
      flex: 1
    },
    navBar: {
      height: 55,
      backgroundColor: 'white',
      elevation: 3,
      paddingHorizontal: 15,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between'
    },
    rightNav: {
      flexDirection: 'row'
    },
    navItem: {
      marginLeft: 25
    },
    body: {
      flex: 1
    },
    tabBar: {
        marginTop:60,
        borderBottomEndRadius: 10,
      backgroundColor: 'white',
      height: 30,
      borderTopWidth: 0.5,
      borderBottomColor: "black",
      borderColor: '#E5E5E5',
      flexDirection: 'row',
      justifyContent: 'space-around',
      marginBottom: 10,
    },
    tabItem: {
        borderBottomColor: "black",
      alignItems: 'center',
      justifyContent: 'center'
    },
    tabTitle: {
      fontSize: 11,
      color: '#3c3c3c',
      paddingTop: 4
    },
    descContainer: {
        flexDirection: 'row',
        paddingTop: 15
    },
})