import React,{ Component } from "react";
import { 
    View,
    Container,
    Header,
    Item,
    Icon,
    Input
 } from "native-base";

export default class Ndas extends Component{
     render(){
         return(
            <View>
            <Container>
                <Header rounded  >
                <Item>
                    <Icon name="ios-camera" style={{fontSize:40, color: "white",}} />
                    
                    <Item style={{width:220, height: 40}}>
                        <Icon active name='search' style={{color: "white",}} />
                        <Input placeholder='Search' style={{color:'white'}}/>
                    </Item>
                    <Icon name="ios-chatbubbles" style={{fontSize:40,marginLeft:10, color: "white",}} />
                    
                </Item>
            </Header> 
            
            </Container>
            
            </View>
         )
     }
 }