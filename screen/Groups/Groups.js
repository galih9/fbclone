import React,{ Component } from 'React';
import {
    View,
    TextInput,
    StyleSheet,
    TouchableOpacity,
    Image,
    ScrollView,
} from 'react-native';
import {
    Container,
    Header,
    Text,
    Item,
    Input,
    Icon,
    Button,
    Content,
    Body,
    Tab,
    Tabs,
    TabHeading,
    Card,
    CardItem
} from 'native-base';
import {
    Divider
} from 'react-native-elements';

import styles from '../Home/hstyle';

class GroupScreen extends Component{
    render(){
        return(
            <View>
                <View>
                <Container>
                    <Header rounded  >
                    <Item>
                        <Icon name="ios-camera" style={{fontSize:40, color: "white",}} />
                        
                        <Item style={{width:220, height: 40}}>
                            <Icon active name='search' style={{color: "white",}} />
                            <Input placeholder='Search' style={{color:'white'}}/>
                        </Item>
                        <Icon name="ios-chatbubbles" style={{fontSize:40,marginLeft:10, color: "white",}} />
                        
                    </Item>
                </Header> 
                
                </Container>
                
                </View>
                <View style={styles.tabBar}>
                <TouchableOpacity style={styles.tabItem} onPress={() => this.props.navigation.navigate('Feeds')}>
                    <Image source={require('../img/home-inactive.png')} style={{width:30,height:30}} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.tabItem} onPress={() => this.props.navigation.navigate('Group')}>
                    <Image source={require('../img/group-active.png')} style={{width:30,height:30}} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.tabItem} onPress={() => this.props.navigation.navigate('Watch')}>
                    <Image source={require('../img/video-inactive.png')} style={{width:30,height:30}} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.tabItem} onPress={() => this.props.navigation.navigate('Profil')}>
                    <Image source={require('../img/prof-inactive.png')} style={{width:30,height:30}} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.tabItem} onPress={() => this.props.navigation.navigate('Notif')}>
                    <Image source={require('../img/bell-inactive.png')} style={{width:40,height:40}} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.tabItem} onPress={() => this.props.navigation.navigate('Menu')}>
                    <Image source={require('../img/menu-inactive.png')} style={{width:25,height:25}} />
                </TouchableOpacity>
                </View>
                
                <Divider style={{backgroundColor:'#9ea2a8',height:1}}/>
                <ScrollView>

                </ScrollView>
                
            </View>
            
        )
    }
}
export default GroupScreen