import React, { Component } from 'react';
import { View } from 'react-native';
import { Container, Header, Content, Tab, Tabs } from 'native-base';

import MenuScreen from './Menu/Menu';
import ProfilScreen from './Profile/Profile';
import FeedsScreen from './Home/Feeds';
export default class IndexScreen extends Component {
  render() {
    return (
    <View>
      <Container>
        <Header hasTabs />
        <Tabs>
          <Tab>
            <FeedsScreen />
          </Tab>
          <Tab>
            <MenuScreen />
          </Tab>
          <Tab>
            <ProfilScreen />
          </Tab>
        </Tabs>
      </Container>
    </View> 
    );
  }
}