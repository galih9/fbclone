var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    bodyParser = require('body-parser'),
    controller = require('./controller');
 
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const axios = require('axios');
var routes = require('./routes');
routes(app);

axios.get('localhost:3000/user', {
    params: {
      ID: 1
    }
  })
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  })
  .then(function () {
    // always executed
  });  


app.listen(port);
console.log('Learn Node JS, RESTful API server running on Port ' + port);