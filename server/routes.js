'use strict';

module.exports = function(app) {
    var todoList = require('./controller');

    app.route('/')
        .get(todoList.index);

    app.route('/api/posts')
        .get(todoList.posts);

    app.route(`/api/posts/:post_id`)
        .get(todoList.findPosts);
    app.route('/api/posts')
        .post(todoList.createPosts);

    app.route('/api/posts')
        .put(todoList.updatePosts);
    
    app.route('/api/posts')
        .delete(todoList.deletePosts);
};