'use strict';

var response = require('./res');
var connection = require('./conn');

exports.posts = function(req, res) {
    connection.query('SELECT * FROM post', function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok(rows, res)
        }
    });
};

exports.index = function(req, res) {
    response.ok("Hello selamat datang di index!", res)
};



exports.findPosts = function(req, res) {
    
    var post_id = req.params.post_id;
    connection.query('SELECT * FROM post where postID = ?',
    [ post_id ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok(rows, res)
        }
    });
};

exports.createPosts = function(req, res) {
    
    var name = req.body.name;
    var capt = req.body.capt;
    var like = req.body.like;
    var image = req.body.image;
    var avatar =req.body.avatar;

    connection.query('INSERT INTO post (username, caption, image, avatar, likeCount) values (?,?,?,?,?)',
    [ name, capt, image, avatar, like ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok("Berhasil menambahkan post!", res)
        }
    });


    // let data = {product_name: req.body.product_name, product_price: req.body.product_price};
    // let sql = "INSERT INTO post SET ?";
    // let query = conn.query(sql, data,(err, results) => {
    //   if(err) throw err;
    //   res.redirect('/');
    // });
};

exports.updatePosts = function(req, res) {
    
    var postid = req.body.postid;
    var name = req.body.name;
    var capt = req.body.capt;
    var like = req.body.like;
    var image = req.body.image;
    var avatar =req.body.avatar;

    connection.query('UPDATE post SET username = ?, caption = ?,avatar = ?,image = ?,likeCount = ? WHERE postID = ?',
    [ name, capt, avatar, image, like, postid ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok("Berhasil merubah post!", res)
        }
    });
};

exports.deletePosts = function(req, res) {
    
    var postid = req.body.postid;

    connection.query('DELETE FROM post WHERE postID = ?',
    [ postid ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok("Berhasil menghapus post!", res)
        }
    });
};